
#include <QUdpSocket>
#include <QDebug>

#include "udpserver.h"

UdpServer::UdpServer(quint16 port, QHostAddress host, QObject *parent)
    : QObject(parent), _host(host), _port(port)
{
    _socket = new QUdpSocket(this);
    _socket->bind(_host, _port);

    connect(_socket, &QUdpSocket::readyRead, this, &UdpServer::readyRead);
    connect(_socket, &QUdpSocket::bytesWritten, this, &UdpServer::bytesWritten);
}

void UdpServer::write(QByteArray &data, QHostAddress &host, quint16 peerPort)
{
    _socket->writeDatagram(data, host, peerPort);
}

QHostAddress &UdpServer::getHost()
{
    return _host;
}

quint16 UdpServer::getPort()
{
    return _port;
}

void UdpServer::readyRead()
{
    QByteArray buffer;
    buffer.resize(static_cast<int>(_socket->pendingDatagramSize()));

    QHostAddress sender;
    quint16 senderPort;

    _socket->readDatagram(buffer.data(), buffer.size(), &sender, &senderPort);

    qDebug() << "data received from " << sender;

    emit dataReceived(buffer, sender, senderPort);
}

void UdpServer::bytesWritten(qint64 bytes)
{
    emit dataWritten(bytes);
}
