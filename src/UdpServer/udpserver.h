#ifndef UDPSERVER_H
#define UDPSERVER_H

#include <QObject>
#include <QUdpSocket>

class UdpServer : public QObject
{
    Q_OBJECT
public:
    explicit UdpServer(quint16 port, QHostAddress host = QHostAddress::LocalHost, QObject *parent = nullptr);
    void write(QByteArray& data, QHostAddress& host, quint16 peerPort);
    QHostAddress &getHost();
    quint16 getPort();

signals:
    void dataReceived(QByteArray& data, QHostAddress& sender, quint16& senderPort);
    void dataWritten(qint64 len);

private slots:
    void readyRead();
    void bytesWritten(qint64 bytes);

private :
    QUdpSocket * _socket;
    QHostAddress _host;
    quint16 _port;
};

#endif // UDPSERVER_H
