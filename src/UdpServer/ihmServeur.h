#ifndef IHMSERVEUR_H
#define IHMSERVEUR_H

#include <QDialog>
#include <QNetworkInterface>

#include "udpserver.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainUI; }
QT_END_NAMESPACE

class MainUI : public QDialog
{
    Q_OBJECT

public:
    MainUI(QWidget *parent = nullptr);
    ~MainUI();

private:
    Ui::MainUI *ui;
    UdpServer * _server;
    QList<QHostAddress> _hostAddrs;

private slots:
    void onDataReceived(QByteArray& data, QHostAddress& sender, quint16& senderPort);
    void on_btnLancer_clicked();
};
#endif // IHMSERVEUR_H
