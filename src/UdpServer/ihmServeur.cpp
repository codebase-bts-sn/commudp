#include <QNetworkInterface>

#include "ihmServeur.h"
#include "ui_ihmServeur.h"

MainUI::MainUI(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainUI)
{
    ui->setupUi(this);

    QList<QNetworkInterface> ifs = QNetworkInterface::allInterfaces();

    ui->cbxInterface->addItem("loopback - 127.0.0.1/8");
    _hostAddrs.append(QHostAddress::LocalHost);

    for(int i = 0; i < ifs.count(); i++)
    {
        if ( (ifs[ i ].type() == QNetworkInterface::Ethernet)
             || (ifs[ i ].type() == QNetworkInterface::Wifi)
             )
        {
            QList<QNetworkAddressEntry> addrs = ifs[ i ].addressEntries();

            for(int j = 0; j < addrs.count(); j++) {
                if( addrs[ j ].ip().protocol() == QAbstractSocket::IPv4Protocol) {
                    qDebug() << ifs[ i ].humanReadableName() << " - " << addrs[ j ].ip().toString() << "/" << addrs[ j ].prefixLength();
                    QString ifDesc = ifs[ i ].humanReadableName()
                            + " - "
                            + addrs[ j ].ip().toString()
                            + "/"
                            + QString::number(addrs[ j ].prefixLength())
                            ;
                    ui->cbxInterface->addItem(ifDesc);
                    _hostAddrs.append(addrs[ j ].ip());
                }
            }
        }
    }
    ui->cbxInterface->setCurrentIndex(0);
}

MainUI::~MainUI()
{
    delete ui;
}

void MainUI::onDataReceived(QByteArray& data, QHostAddress& sender, quint16& senderPort)
{
    QString log;

    log.append("(from ");
    log.append(sender.toString());
    log.append(":");
    log.append(QString::number(senderPort));
    log.append(") : ");
    log.append(QString(data.data()).toLatin1());

    ui->tedtLog->append(log);

    _server->write(QByteArray().append("OK"), sender, senderPort);
}

void MainUI::on_btnLancer_clicked()
{
    if( ui->btnLancer->isChecked() ) {
        _server = new UdpServer(ui->ledtPort->text().toInt()
            , _hostAddrs[ ui->cbxInterface->currentIndex() ]
            );
        connect(_server, &UdpServer::dataReceived, this, &MainUI::onDataReceived);
        ui->btnLancer->setText("Stopper");
        ui->grpbxConfig->setEnabled(false);
    } else {
        delete _server;
        ui->grpbxConfig->setEnabled(true);
        ui->btnLancer->setText("Lancer");
    }
}

