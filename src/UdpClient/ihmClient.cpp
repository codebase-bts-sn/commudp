#include "ihmClient.h"
#include "ui_ihmClient.h"

MainUI::MainUI(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainUI)
{
    ui->setupUi(this);
    QHostAddress host = QHostAddress::LocalHost;
    ui->ledtHost->setText(host.toString());
}

MainUI::~MainUI()
{
    delete ui;
}


void MainUI::on_btnConnecter_clicked()
{
    if( ui->btnConnecter->isChecked() ) {
        _client = new UdpClient(QHostAddress(ui->ledtHost->text()));

        connect(_client, &UdpClient::dataWritten, this, &MainUI::onDataWritten);
        connect(_client, &UdpClient::dataReceived, this, &MainUI::onDataReceived);

        ui->ledtHost->setEnabled(false);
        ui->ledtPort->setEnabled(false);
        ui->btnEnvoyer->setEnabled(true);
        ui->btnConnecter->setText("Stopper communication");
    } else {
        delete _client;

        ui->ledtHost->setEnabled(true);
        ui->ledtPort->setEnabled(true);
        ui->btnEnvoyer->setEnabled(false);
        ui->btnConnecter->setText("Débuter communication");
    }
}


void MainUI::on_btnEnvoyer_clicked()
{
    QByteArray data;
    _client->write(data.append(ui->ledtDatagram->text().toLatin1()), _PORT);
}

void MainUI::onDataWritten(qint64 len)
{
    QString sentData = ui->ledtDatagram->text();
    sentData.truncate(len);

    ui->tedtLog->setTextColor(QColor( "green" ));
    ui->tedtLog->append(">> " + sentData);
}

void MainUI::onDataReceived(const QByteArray &data)
{
    ui->tedtLog->setTextColor(QColor( "blue" ));
    ui->tedtLog->append(QString("<< ").append(data.data()));

}

