#include <QDebug>
#include <QUdpSocket>

#include "udpclient.h"

UdpClient::UdpClient(QHostAddress host, QObject *parent)
    : QObject(parent), _host(host)
{
    bool ret;

    _socket = new QUdpSocket(this);

    connect(_socket, &QUdpSocket::readyRead, this, &UdpClient::readyRead);
    connect(_socket, &QUdpSocket::bytesWritten, this, &UdpClient::bytesWritten);

}

void UdpClient::write(QByteArray &data, quint16 port)
{
    _socket->writeDatagram(data, _host, port);
}

QHostAddress &UdpClient::getHost()
{
    return _host;
}

quint16 UdpClient::getPort()
{
    return _port;
}

void UdpClient::readyRead()
{
    QByteArray buffer;
    buffer.resize(_socket->pendingDatagramSize());

    QHostAddress sender;
    quint16 senderPort;

    _socket->readDatagram(buffer.data(), buffer.size(), &sender, &senderPort);

    emit dataReceived(buffer);
}

void UdpClient::bytesWritten(qint64 bytes)
{
    emit dataWritten(bytes);
}
