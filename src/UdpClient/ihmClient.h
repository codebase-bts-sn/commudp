#ifndef IHMCLIENT_H
#define IHMCLIENT_H

#include <QDialog>
#include <udpclient.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainUI; }
QT_END_NAMESPACE

class MainUI : public QDialog
{
    Q_OBJECT

public:
    MainUI(QWidget *parent = nullptr);
    ~MainUI();

private slots:
    void on_btnConnecter_clicked();
    void on_btnEnvoyer_clicked();
    void onDataWritten(qint64 len);
    void onDataReceived(const QByteArray& data);

private:
    Ui::MainUI *ui;
    UdpClient * _client;
    const quint16 _PORT = 1234;
};
#endif // IHMCLIENT_H
