#ifndef UDPCLIENT_H
#define UDPCLIENT_H

#include <QObject>
#include <QUdpSocket>

class UdpClient : public QObject
{
    Q_OBJECT
public:
    explicit UdpClient(QHostAddress host = QHostAddress::LocalHost, QObject *parent = nullptr);
    void write(QByteArray& data, quint16 port);
    QHostAddress &getHost();
    quint16 getPort();

signals:
    void dataReceived(const QByteArray& data);
    void dataWritten(qint64 len);

private slots:
    void readyRead();
    void bytesWritten(qint64 bytes);

private :
    QUdpSocket * _socket;
    QHostAddress _host;
    int _port;

};

#endif // UDPCLIENT_H
